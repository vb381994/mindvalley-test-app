package com.example.vb.mindvalleytestapp.adapters;

import android.app.Activity;
import android.app.Notification;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.example.vb.mindvalleytestapp.R;
import com.example.vb.mindvalleytestapp.ws.models.UserData;

import java.util.List;

public class UserDataAdapter extends RecyclerView.Adapter<UserDataAdapter.MyViewHolder> {

    private Activity mContext;
    private List<UserData> userDataList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvUserName;
        private ImageView ivCover;
        private de.hdodenhof.circleimageview.CircleImageView ivProfileImage;

        private MyViewHolder(View view) {
            super(view);
            ivProfileImage = view.findViewById(R.id.ivProfileImage);
            ivCover = view.findViewById(R.id.ivCover);
            tvUserName = view.findViewById(R.id.tvUserName);
        }
    }

    public UserDataAdapter(Activity mContext, List<UserData> userDataList) {
        this.mContext = mContext;
        this.userDataList = userDataList;
    }

    @Override
    public UserDataAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.raw_data_card, parent, false);

        return new UserDataAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final UserDataAdapter.MyViewHolder holder, final int position) {

        UserData data = userDataList.get(position);

        Glide.with(mContext)
                .load(data.getUser().getProfile_image().getSmall())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.ivProfileImage);
        Glide.with(mContext)
                .load(data.getUrls().getRaw())
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.ivCover);

        holder.tvUserName.setText(data.getUser().getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

    }


    @Override
    public int getItemCount() {
        return userDataList.size();
//        return 20;
    }

}
