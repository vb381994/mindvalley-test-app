package com.example.vb.mindvalleytestapp.ws.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

public class Utility {
    private static ProgressDialog progressDialog;

    // This function is to check in internet connection
    public static boolean isConnected(Activity activity) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    //To launch simple intent
    public static void launchActivity(Activity activity, Class classToOpen) {
        Intent intent = new Intent(activity, classToOpen);
        activity.startActivity(intent);
    }

    //To launch simple intent with web url
    public static void launchActivityWithWebUrl(Activity activity, Class classToOpen, String webUrl) {
        Intent intent = new Intent(activity, classToOpen);
        intent.putExtra(webUrl, webUrl);
        activity.startActivity(intent);
    }

    //To launch activity with clear stack
    public static void launchActivityWithClearStack(Activity activity, Class classToOpen) {
        Intent intent = new Intent(activity, classToOpen);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

    // Show Toast
    public static void showToast(Activity activity, String msg) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
    }

    public static void createProgressDialog(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
    }

    public static void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private static String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public static String urlEncodeUTF8(Map<?, ?> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s",
                    urlEncodeUTF8(entry.getKey().toString()),
                    urlEncodeUTF8(entry.getValue().toString())
            ));
        }
        if (sb.length() > 0) {
            sb.insert(0, "?");
        }
        return sb.toString();
    }


}
