package com.example.vb.mindvalleytestapp.ws;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import com.android.volley.toolbox.StringRequest;
import com.example.vb.mindvalleytestapp.ws.controllers.ApplicationClass;
import com.example.vb.mindvalleytestapp.ws.interfaces.VolleyResponseListener;
import com.example.vb.mindvalleytestapp.ws.models.UserData;
import com.example.vb.mindvalleytestapp.ws.utils.Utility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sanjay.
 */

public class VolleyService {

    private static final String TAG = "Response";

    public static <T> void GetMethod(final String url, final Class<T> modelClass, final HashMap<String, String> param,
                                     final VolleyResponseListener listener) {

        String finalUrl = url;
        if (param != null && param.size() != 0) {
            finalUrl = url + Utility.urlEncodeUTF8(param);
        }

        // Initialize a new StringRequest
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                finalUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jobj=new JSONObject();
                            JSONArray array=new JSONArray(response);
                            jobj.put("response",array);
//                            T responseObject = gson.fromJson(jobj.toString(), modelClass);
                            listener.onResponse(jobj, url, true, null);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onResponse(null, url, false, error);
                    }
                });

        // Access the RequestQueue through singleton class.
        ApplicationClass.getInstance().addToRequestQueue(stringRequest);
    }



    public static <T> void PostMethod(final String url, final Class<T> modelClass,
                                      final HashMap<String, String> headers, final VolleyResponseListener listener) {

        // Initialize a new StringRequest
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.e(TAG, "onResponse: " + response);
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        T responseObject = gson.fromJson(response, modelClass);
                        listener.onResponse(responseObject, url, true, null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onResponse(null, url, false, error);
                    }
                })

        {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return headers;
            }


        };

        // Access the RequestQueue through singleton class.
        ApplicationClass.getInstance().addToRequestQueue(stringRequest);
    }

    public static <T> void PostMethodWithString(final String requestBody, final String url, final Class<T> modelClass,
                                                final VolleyResponseListener listener) {

        // Initialize a new StringRequest
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.e(TAG, "onResponse: " + response);
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        try {
                            JSONObject jobj = new JSONObject(response);
                            T responseObject = gson.fromJson(jobj.toString(), modelClass);
                            listener.onResponse(responseObject, url, true, null);
                        } catch (JSONException e) {
                            Log.e(TAG, "onResponse: CALLEDDD::::");
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onResponse(null, url, false, error);
                    }
                }) {


            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                String responseString = "";
                if (response != null) {
                    responseString = String.valueOf(response);
                    // can get more details such as response.headers
                }
                return Response.success(responseString, HttpHeaderParser.parseCacheHeaders(response));
            }


        };

        // Access the RequestQueue through singleton class.
        ApplicationClass.getInstance().addToRequestQueue(stringRequest);
    }

    public static <T> void PostMethodWithJSON(final String url, final Class<T> modelClass,
                                              final JSONObject jsonObject,
                                              final VolleyResponseListener listener) {

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        Log.e(TAG, "onResponse::: " + response);
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        T responseObject = gson.fromJson(response.toString(), modelClass);
                        listener.onResponse(responseObject, url, true, null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        Log.e(TAG, "ERROR::: " + error.toString());
                        listener.onResponse(null, url, false, error);
                    }
                }) {

//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                return headers;
//            }


        };

        // Access the RequestQueue through singleton class.
        ApplicationClass.getInstance().addToRequestQueue(jsonObjectRequest);
    }


    public static <T> void PostMethodWithHeaders(final String url, final Class<T> modelClass,
                                                 final HashMap<String, String> params, final HashMap<String, String> headers,
                                                 final VolleyResponseListener listener) {

        // Initialize a new StringRequest
        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.e(TAG, "onResponse: " + response);
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        T responseObject = gson.fromJson(response, modelClass);
                        listener.onResponse(responseObject, url, true, null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        listener.onResponse(null, url, false, error);
                    }
                })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            /**
             * Passing some request headers
             * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }

        };

        // Access the RequestQueue through singleton class.
        ApplicationClass.getInstance().addToRequestQueue(stringRequest);
    }
}
