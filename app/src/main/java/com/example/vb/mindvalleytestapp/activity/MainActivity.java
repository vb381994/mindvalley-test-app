package com.example.vb.mindvalleytestapp.activity;

import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.VolleyError;
import com.example.vb.mindvalleytestapp.R;
import com.example.vb.mindvalleytestapp.adapters.UserDataAdapter;
import com.example.vb.mindvalleytestapp.constants.AppConstants;
import com.example.vb.mindvalleytestapp.constants.WebConstants;
import com.example.vb.mindvalleytestapp.ws.VolleyService;
import com.example.vb.mindvalleytestapp.ws.interfaces.VolleyResponseListener;
import com.example.vb.mindvalleytestapp.ws.models.UserData;
import com.example.vb.mindvalleytestapp.ws.utils.EndlessRecyclerOnScrollListenerNewGrid;
import com.example.vb.mindvalleytestapp.ws.utils.Utility;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity
        implements VolleyResponseListener, SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView rvData;
    private ProgressBar pb;
    private UserDataAdapter adapter;
    private List<UserData> userDataList;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupControls();
    }

    //Initialize all controls
    private void setupControls() {

        rvData = findViewById(R.id.rvData);
        pb = findViewById(R.id.pb);
        userDataList = new ArrayList<>();
        mSwipeRefreshLayout = findViewById(R.id.swipeToRefresh);
        adapter = new UserDataAdapter(MainActivity.this, userDataList);
        initGlobal();
    }


    //execute the all main functions
    private void initGlobal() {
        rvData.setVisibility(View.GONE);
        //Layout Manager for recyclerview
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        //assign layout manager to recyclerview
        rvData.setLayoutManager(layoutManager);
        //set adapter to recyclerview
        rvData.setAdapter(adapter);

        mSwipeRefreshLayout.setOnRefreshListener(this);

        rvData.setOnScrollListener(new EndlessRecyclerOnScrollListenerNewGrid(layoutManager) {
            @Override
            public void onLoadMore(int paramInt) {
                Utility.showToast(MainActivity.this, "Loading more data!");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //load more data
                        loadMoreData();
                    }
                }, 1000);

            }
        });
        callServiceToGetAllUserData();
    }

    private void loadMoreData() {
        Collections.shuffle(userDataList, new Random(System.nanoTime()));
        userDataList.addAll(userDataList);
        adapter.notifyDataSetChanged();
    }

    private void callServiceToGetAllUserData() {
        if (Utility.isConnected(this)) {
            VolleyService.GetMethod(WebConstants.BASE_URL, UserData.class, null, this);
        } else {
            pb.setVisibility(View.GONE);
            Utility.showToast(this, AppConstants.NO_NET);
        }
    }

    @Override
    public void onResponse(Object response, String url, boolean isSuccess, VolleyError error) {
        switch (url) {
            case WebConstants.BASE_URL:
                if (isSuccess) {
                    // Got the response successfully

                    try {
                        JSONObject data = new JSONObject(String.valueOf(response));
                        JSONArray array = data.getJSONArray("response");
                        //set all data
                        setAllUserData(array);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    //Didn't get response
                    Utility.showToast(this, AppConstants.SERVER_ERROR);
                }
                break;
        }
    }

    private void setAllUserData(JSONArray array) {
        UserData userData;
        userDataList.clear();
        final GsonBuilder gsonBuilder = new GsonBuilder();
        final Gson gson = gsonBuilder.create();
        // add all userdata object with loop
        for (int a = 0; a < array.length(); a++) {
            try {
                userData = gson.fromJson(array.getJSONObject(a).toString(), UserData.class);
                userDataList.add(userData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
        adapter.notifyDataSetChanged();
        pb.setVisibility(View.GONE);
        rvData.setVisibility(View.VISIBLE);

    }

    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);
        callServiceToGetAllUserData();
    }
}
