package com.example.vb.mindvalleytestapp.activity;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;

import com.example.vb.mindvalleytestapp.R;
import com.example.vb.mindvalleytestapp.ws.utils.Utility;

public class SplashActivity extends AppCompatActivity {

    // Splash screen timer
    private int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        setupControls();
    }

    //Initialize all controls
    private void setupControls() {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();


        initGlobal();
    }

    //execute the all main functions
    private void initGlobal() {
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Utility.launchActivity(SplashActivity.this, MainActivity.class);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
