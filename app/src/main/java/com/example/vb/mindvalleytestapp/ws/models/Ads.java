package com.example.vb.mindvalleytestapp.ws.models;

/**
 * Created by VB on 10/1/2017.
 */

public class Ads {
    public String banner_id;
    public String native_id;
    public String interstrial_id;

    public String getBanner_id() {
        return banner_id;
    }

    public void setBanner_id(String banner_id) {
        this.banner_id = banner_id;
    }

    public String getNative_id() {
        return native_id;
    }

    public void setNative_id(String native_id) {
        this.native_id = native_id;
    }

    public String getInterstrial_id() {
        return interstrial_id;
    }

    public void setInterstrial_id(String interstrial_id) {
        this.interstrial_id = interstrial_id;
    }
}
