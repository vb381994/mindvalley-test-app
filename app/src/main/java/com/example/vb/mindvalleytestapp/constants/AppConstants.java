package com.example.vb.mindvalleytestapp.constants;

public class AppConstants {
    public static final String NO_NET = "No Internet Connection!!!Please try again later.";
    public static final String SERVER_ERROR = "Failed to get data! Please try again later.";
}
